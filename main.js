import Vue from 'vue'
import App4 from './App4.vue'
import Buefy from "buefy"
import "buefy/dist/buefy.css"

Vue.use(Buefy)
Vue.config.productionTip = false

new Vue({
  render: h => h(App4),
}).$mount('#app4')
